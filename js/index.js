$(function () {
				$('[data-toggle="tooltip"]').tooltip()
				$('[data-toggle="popover"]').popover({
        		placement : 'top',
        		trigger : 'hover'
    			});
    			$('.carousel').carousel({
    				interval: 2000
    			});

    			$('#reservarhotel').on('show.bs.modal', function (e){
    				console.log('el modal reserva se esta mostrando');

    				$('#reservarhotelBtn').removeClass('btn-primary');
    				$('#reservarhotelBtn').addClass('btn-outline-success');
    				$('#reservarhotelBtn').prop('disabled', true);
    				
    			});

    			$('#reservarhotel').on('shown.bs.modal', function (e){
    				console.log('el modal reserva se mostró');

    			});
    			$('#reservarhotel').on('hide.bs.modal', function (e){
    				console.log('el modal reserva se oculta');
    			});
    			$('#reservarhotel').on('hidden.bs.modal', function (e){
    				console.log('el modal reserva se ocultó');
    				$('#reservarhotelBtn').removeClass('btn-outline-success');
    				$('#reservarhotelBtn').addClass('btn-primary');
    				$('#reservarhotelBtn').prop('disabled', false);
    			});
			})